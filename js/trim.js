const LENGHT = 35;

function limitString(str) {
  len = str.length;
  if (len > LENGHT) {
    return str.substr(0, (LENGHT - 3)) + '...';
  } else {
    return str;
  }
}

document.querySelectorAll('.trim-that a').forEach(function (i) {
  i.textContent = limitString(i.textContent);
});

document.querySelectorAll('.limit').forEach(function (i) {
  i.textContent = limitString(i.textContent);
});