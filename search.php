<?php get_header(); ?> <!-- ouvrir header,php -->
<main id="skip">
    <h1 class="page-title"><?php _e('', 'sandbox') ?> Recherche pour le terme « <?php echo get_search_query(); ?> »</h1>

    <?php include(TEMPLATEPATH . '/components/preview-list.php'); ?>
</main>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
