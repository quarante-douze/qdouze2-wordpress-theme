<?php
/////////////////////////////////////////////////////////////////////////////
// Reading time
// Return the average reading time of an article
//
// Version 0.1

function reading_time() {
  $content = get_post_field( 'post_content', $post->ID );
  $word_count = str_word_count( strip_tags( $content ) );
  $readingtime = ceil($word_count / 200);

  if ($readingtime == 1) {
  $timer = " minute";
  } else {
  $timer = " minutes";
  }
  $totalreadingtime = $readingtime . $timer;

  return $totalreadingtime;
}

function image_number() {
  $content = get_post_field( 'post_content', $post->ID );
  $count = substr_count( $content, '<img' );
  

  if ($count == 1) {
  $timer = " image";
  } else {
  $timer = " images";
  }
  $imagenumber = $count . $timer;

  return $imagenumber;
}

?>