<?php
/////////////////////////////////////////////////////////////////////////////
// Posts supports
// Activate the different elements supported by the posts
//
// Version 0.1

// Support des extraits
 function kspace_post_supports() {
 	add_post_type_support( 'post', 'excerpt');
 }

 add_action( 'init', 'kspace_post_supports' );

 add_theme_support( 'post-thumbnails' );
 set_post_thumbnail_size( 520, 290, true );
 add_image_size( 'post-size', 816, 459, true );


?>
