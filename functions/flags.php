<?php
/////////////////////////////////////////////////////////////////////////////
// Flags supports
// Allow to show a banner with a custom message, via a special taxonomy
//
// Version 0.1

// Register the taxonomy
register_taxonomy(
  'flag',
  'post',
  array(
    'label' => 'Flag',
      'labels' => array(
      'name' => 'Flags',
      'singular_name' => 'Flag',
      'all_items' => 'Tous les flags',
      'edit_item' => 'Éditer le flag',
      'view_item' => 'Voir le flag',
      'update_item' => 'Mettre à jour le flag',
      'add_new_item' => 'Ajouter un flag',
      'new_item_name' => 'Nouveau flag',
      'search_items' => 'Rechercher parmi les flags',
      'popular_items' => 'Flags les plus utilisés'
    ),
  'hierarchical' => false
  )
);

register_taxonomy_for_object_type( 'flag', 'post' );

// Add field to be able to change the flags
function flags_taxonomy_custom_fields($tag) {
   // Check for existing taxonomy meta for the term you're editing
    $t_id = $tag->term_id; // Get the ID of the term you're editing
    $term_meta = get_option( "taxonomy_term_$t_id" ); // Do the check
?>

<tr class="form-field">
    <th scope="row" valign="top">
        <label for="niveau"><?php _e('Niveau du flag'); ?></label>
    </th>
    <td>
        <input type="text" name="term_meta[niveau]" id="term_meta[niveau]" size="25" style="width:60%;" value="<?php echo $term_meta['niveau'] ? $term_meta['niveau'] : ''; ?>"><br />
        <span class="description"><?php _e('Le niveau du flag (info, warning, danger…)'); ?></span>
    </td>
</tr>

<?php
}

function flags_taxonomy_custom_fields_add( $taxonomy ) {
   // Check for existing taxonomy meta for the term you're editing
    $t_id = $tag->term_id; // Get the ID of the term you're editing
    $term_meta = get_option( "taxonomy_term_$t_id" ); // Do the check
?>

<div class="form-field">
  <label for="term_meta[niveau]"><?php _e('Niveau du flag'); ?></label>
  <input type="text" name="term_meta[niveau]" id="term_meta[niveau]" />
  <p><?php _e('Le niveau du flag (info, warning, danger…)'); ?>.</p>
</div>

<?php
}

add_action( 'post_tag_edit_form_fields', 'kspace_edit_term_fields', 10, 2 );

// Sauvegarde du niveau du flag
function save_taxonomy_custom_fields( $term_id ) {
    if ( isset( $_POST['term_meta'] ) ) {
        $t_id = $term_id;
        $term_meta = get_option( "taxonomy_term_$t_id" );
        $cat_keys = array_keys( $_POST['term_meta'] );
            foreach ( $cat_keys as $key ){
            if ( isset( $_POST['term_meta'][$key] ) ){
                $term_meta[$key] = $_POST['term_meta'][$key];
            }
        }
        //save the option array
        update_option( "taxonomy_term_$t_id", $term_meta );
    }
}

// Ajout des fields
add_action( 'flag_edit_form_fields', 'flags_taxonomy_custom_fields', 10, 2 );
add_action( 'flag_add_form_fields', 'flags_taxonomy_custom_fields_add' );
add_action( 'created_flag', 'save_taxonomy_custom_fields' );
add_action( 'edited_flag', 'save_taxonomy_custom_fields', 10, 2 );

?>