<?php
/* functions.php
 *
 * All the function of Quarante-Douze
 *
 */


 /* 0. Excerpt and thumbnail support
  *
  * Ces fonctions ajoutent les fonctionnalités de base du theme,
  * tel que les extraits et les miniatures.
  */

include(TEMPLATEPATH . '/functions/posts-support.php');
include(TEMPLATEPATH . '/functions/niveaux.php');
include(TEMPLATEPATH . '/functions/flags.php');
include(TEMPLATEPATH . '/functions/breadcrumb.php');
include(TEMPLATEPATH . '/functions/reading-time.php');
include(TEMPLATEPATH . '/functions/remove-block-library.php');

  function wpb_custom_new_menu() {
    register_nav_menus(
      array(
        'top-navbar' => __( 'Navbar (gauche)' ),
        'top-navbar-2' => __( 'Navbar (secondaire)' ),
        'link-menu' => __( 'Liste des liens' ),
        'footer-pages' => __( 'Pages dans le footer' ),
        'social' => __( 'Reseaux sociaux' ),
        'social-plus' => __( 'Reseaux sociaux supplémentaires' ),
      )
    );
  }
  add_action( 'init', 'wpb_custom_new_menu' );
