<?php get_header(); ?>
<main id="skip">
  <div class="card c-danger">
    <div class="card-header">404 Not Found</div>
    <div class="card-body">
      <p>La page que vous cherchez n'a pas été trouvée</p>
      <p>Veuillez vérifier l'adresse, si le cas recommence, n'hésitez pas à contacter le webmestre via kazhnuz [at] kobold [point] cafe</p>
    </div>
  </div>
</main>
<?php get_sidebar(); ?>
<?php get_footer();  ?>
