<div id="featured-articles">
  <?php
  query_posts(array( 
    'post_type' => 'post',
    'tax_query' => array(
        array(
            'taxonomy' => 'niveau',
            'terms' => 'featured',
            'field' => 'slug',
        )
    ),
    'orderby' => 'date',
    'posts_per_page' => '3',
    'order' => 'DESC' 
  ));
  
  if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
  
  <article class="preview-featured">
    <a href="<?php the_permalink(); ?>" class="preview-link">
      <div class="preview-item" style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>');">
		    <div class="preview-overlay">
		      <div class="preview-categories">
		        <?php $category = get_the_category();
              echo"<span class='badge c-secondary'>" . $category[0]->cat_name . "</span>"; ?>
            </div>
		    
		      <h2 class="preview-title"><?php the_title();/*3*/ ?></h2>
		    </div>
      </div>
		</a>
  </article>
  
  
  <?php endwhile; ?> <?php wp_reset_query(); /*4*/ ?>
</div>
 
 

