<nav class="container menu toolbar bg fg-dark categories d-none d-flex-sm">
  <h2 class="sr-only">Liste des catégories</h2>
  <ul class="f-around">
    <?php
      $categories = get_categories( array(
        'orderby' => 'name',
        'order'   => 'ASC'
      ) );

      foreach( $categories as $category ) {?>
        <li>
          <a class="menu-item" href="<?php echo get_category_link($category->term_id) ?>"><?php echo $category->name ?></a>
        </li>
      <?php }
    ?>
  </ul>
</nav>
