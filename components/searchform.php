<form method="get" id="searchform" action="<?php bloginfo('home'); ?>/">
	<div>
		<label for="searchfield" class="sr-only">Formulaire de recherche</label>
		<input class="form-control" id="searchfield" type="search" placeholder="Chercher sur le site" aria-label="Chercher sur le site" value="<?php the_search_query(); ?>" name="s" id="s" />
	</div>
</form>
