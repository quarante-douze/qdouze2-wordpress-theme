<?php if(have_posts()) : ?>
  <div class="previews-section preview-list">
    <?php include(TEMPLATEPATH . '/components/previews.php'); ?>
  </div>
  <div class="navigation">
    <?php the_posts_pagination(); ?>
  </div>
<?php endif; ?>
