<footer class="bg-dark fg-light" aria-labelledby="title-footer">
  <h1 class="sr-only" id="title-footer">Informations sur le site</h1>
  <nav class="container menu toolbar bg fg-dark" aria-label="title-footer-social">
    <h2 class="sr-only" id="title-footer-social">Me suivre</h2>
    <ul class="f-end">
      <?php
        $listmenu = get_nav_menu_locations();
        $menu = wp_get_nav_menu_items($listmenu['social']);
        if ($menu != null) {
        foreach ($menu as $menuElement) { ?>
          <li><a class="menu-item" rel="me" href="<?php echo $menuElement->url; ?>"><svg class="icon" alt=""><use xlink:href="#icon-<?php echo $menuElement->title; ?>"></use></svg><span class="sr-only">Accéder à mon <?php echo $menuElement->title; ?></span></a></li>
        <?php }
      }
      ?>
      <li><a href="<?php bloginfo('rss2_url'); ?>" class="menu-item"><svg class="icon" alt=""><use xlink:href="#icon-rss"></use></svg><span class="sr-only">Flux RSS du site</span></a></li>
    </ul>
  </nav>
  <div class="container columns">
    <section class="col-12 col-md-4" aria-labelledby="title-footer-section-1">
      <h2 class="sr-only" id="title-footer-section-1">Droit d'utilisations</h2>
      <p>Les contenus sont diffusé sous licence Creative Common Attribution - Partage à l'Identique 4.0 - hors mention contraire.</p>
      <p>Ces licences vous autorise à partager et copier mes travaux, tant que vous me citiez en source, et que vous autorisez la même chose pour les travaux qui en seraient dérivés. N'hésitez pas à partager ! ❤️</p>
    </section>

    <section class="col-12 col-md-4" aria-labelledby="title-footer-section-2">
      <h2 class="sr-only" id="title-footer-section-2">Crédits</h2>
      <p>Ce site est propulsé par <a href="https://wordpress.org">Wordpress</a></p>
      <p>Le <a href="https://git.kobold.cafe/quarante-douze/qdouze2-wordpress-theme">theme wordpress</a> de ce site est disponible sous licence CC BY-SA et GPL v3. Il utilise <a href="https://picturepan2.github.io/spectre/">Spectre</a> et <a href="https://forkawesome.github.io/Fork-Awesome/">Fork-Awesome</a>.</p>
    </section>

    <section class="col-12 col-md-4" aria-labelledby="title-footer-section-3">
      <h2 class="sr-only" id="title-footer-section-3">Informations annexes</h2>
      <p>Toute critique, remarque, etc. est la bienvenue. Pour cela, vous pouvez me contacter à kazhnuz [at] kobold [point] cafe ou sur mes <a href="https://kazhnuz.space/links">réseaux sociaux</a></p>
      <ul aria-label="Pages annexes">
        <?php
          $listmenu = get_nav_menu_locations();
          $menu = wp_get_nav_menu_items($listmenu['footer-pages']);
            foreach ($menu as $menuElement) {
              echo '<li><a href="' . $menuElement->url . '" class="menu-item">'. $menuElement->title . '</a></li>';
            }
          ?>
      </ul>
    </section>
  </div>
</footer>
