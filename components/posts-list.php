<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>
  <div class="card c-primary" id="post-<?php the_ID(); ?>">
    <div class="card-header"><h1><?php the_title(); ?></h1></div>
    <div class="flex-that mb">
        <div class="article-category">
          <?php $category = get_the_category();
            echo"<a href= '" . esc_url( get_category_link( $category[0]->term_id ) ) . "' class='btn btn-small c-info'><svg class='icon' alt=''><use xlink:href='#icon-folder'></use></svg> " . $category[0]->cat_name . "</a>"; ?>
        </div>
        <time itemprop="datePublished"><em><?php the_time('j F Y') ?></em></time>
    </div>
    <?php the_excerpt(); ?>
    <p class="align-center"> <a href="<?php the_permalink(); ?>" class="btn btn-readmore" title="<?php the_title();?>">Lire l'article</a> </p>
  </div>
  <?php endwhile; ?>
  <div class="navigation">
    <?php the_posts_pagination(); ?>
  </div>
<?php endif; ?>
