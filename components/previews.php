<?php while(have_posts()) : the_post(); ?>
  <a href="<?php the_permalink(); ?>" class="preview">
    
    <div class="preview-thumbnail">
      <?php the_post_thumbnail( ); ?>
    </div>
    <div class="preview-text">
      <h2><?php the_title(); ?></h2>
    
      <div class="flex-that">
        <div>
          <?php
            if (!has_category('chapters')) {
              $category = get_the_category();
              echo"<span class='badge c-secondary small-text m-0'>" . $category[0]->cat_name . "</span>";
            }
          ?>
        </div>
        <div>
          <time><span class="badge c-secondary small-text m-0"><?php the_time('d/m/Y') ?></span></time>
        </div>
      </div>
      <div class="preview-excerpt"><?php the_excerpt(); ?></div>
    </div>
  </a>
<?php endwhile; ?>