<nav id="mobile-sidebar" class="bg-dark fg-light sidebar menu hidden">
  <h1 class="align-center text-light">Menu du site</h1>
  <div class="mb-1">
    <form method="get" id="searchform" action="<?php bloginfo('home'); ?>/">
      <div>
        <label for="searchfield2" class="sr-only">Formulaire de recherche</label>
        <input class="form-control" id="searchfield2" type="search" placeholder="Chercher sur le site" aria-label="Chercher sur le site" value="<?php the_search_query(); ?>" name="s" id="s" />
      </div>
    </form>
  </div>
  <h2 id="sidebar-menu-principal-titre">Menu principal</h2>
  <ul aria-labelledby="sidebar-menu-principal-titre">
    <li>
      <a href="<?php echo site_url(); ?>" class="menu-item">
        <span><svg class="icon icon-home" alt=""><use xlink:href="#icon-home"></use></svg> Accueil</span>
      </a>
    </li>
    <?php
      $listmenu = get_nav_menu_locations();
      $menu = wp_get_nav_menu_items($listmenu['top-navbar']);
        foreach ($menu as $menuElement) {
          echo '<li><a href="' . $menuElement->url . '" class="menu-item">'. $menuElement->title . '</a></li>';
        }
      ?>
  </ul>
  <h2 id="sidebar-categories-titre">Liste des catégories</h2>
  <ul aria-labelledby="sidebar-categories-titre">
    <?php
      $categories = get_categories( array(
        'orderby' => 'name',
        'order'   => 'ASC'
      ) );

      foreach( $categories as $category ) {?>
        <li>
          <a class="menu-item" href="<?php echo get_category_link($category->term_id) ?>"><?php echo $category->name ?></a>
        </li>
      <?php }
    ?>
  </ul>
  <h2 id="sidebar-pages-titre">Pages</h2>
  <ul aria-labelledby="sidebar-pages-titre">
    <?php
      $listmenu = get_nav_menu_locations();
      $menu = wp_get_nav_menu_items($listmenu['top-navbar-2']);
        foreach ($menu as $menuElement) {
          echo '<li><a href="' . $menuElement->url . '" class="menu-item">'. $menuElement->title . '</a></li>';
        }
      ?>
  </ul>
</nav>

<button id="mobile-button" class="menu-button"><svg class="icon icon-bars" alt=""><use xlink:href="#icon-bars"></use></svg> <span class="sr-only">Afficher le menu</span></button>