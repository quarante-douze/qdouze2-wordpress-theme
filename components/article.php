<?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>

<?php include(TEMPLATEPATH . '/components/article/flags.php'); ?>

<article class="article container-article mb-1" id="post-<?php the_ID(); ?>">
  <?php if ($haveTitle) { ?>
    <h1 class="page-title"><?php the_title(); ?></h1>
  <?php } else { ?>
    <h1 class="sr-only"><?php the_title(); ?></h1>
  <?php } ?>

  <div class="thumbnail mb-half" aria-hidden="true">
    <?php the_post_thumbnail( 'post-size' ); ?>
  </div>

  <div class="article-entry mb-1">
    <?php the_content(); ?>
  </div>

  <aside class="card card-noheader article-meta">
    <h2 class="sr-only">Métadonnées de l'article</h2>
    <div class="card-body">
      <div class="author-area">
        <?php echo get_avatar( get_the_author_meta('user_email'), $size = '120'); ?>
        <div class="author-metadata">
          <div class="author-pseudo">Écrit par <a href="https://kazhnuz.space"><?php the_author() ?></a></div>
          <small class="author-date">Le <?php the_time('l j F Y à H:i') ?></small>
        </div>
      </div>

      <div class="article-category">
        <?php include(TEMPLATEPATH . '/components/article/terms.php'); ?>
      </div>
    </div>
  </aside>
</article>

<?php endwhile; ?>
<?php endif; ?>
