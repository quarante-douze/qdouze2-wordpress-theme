<section class="card c-secondary d-flex-sm d-none">
  <h2 class="card-header"><svg class="icon icon-folder" alt=""><use xlink:href="#icon-folder"></use></svg> Catégories</h2>
  <div class="menu fg-dark">
    <ul>
      <?php
        $categories = get_categories( array(
          'orderby' => 'name',
          'order'   => 'ASC'
        ) );

        foreach( $categories as $category ) {
          if ($category->slug != "blog" && $category->slug != "chapters") {
            echo '<li><a class="menu-element" href="' . get_category_link($category->term_id) . '">' . $category->name . '<span class="badge bg-primary m0">'. $category->count . '</span></a></li>';
            }
        }?>
    </ul>
  </div>
</section>
