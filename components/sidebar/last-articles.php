<section class="card c-secondary">
  <h2 class="card-header"><svg class="icon" alt=""><use xlink:href="#icon-rss"></use></svg> Publications</h2>
  <div class="menu fg-dark">
    <ul>
    <?php
      wp_get_archives( array(
        'type'  => 'postbypost',
        'echo'  => 1,
        'order' => 'ASC',
        'limit' => 6
      ) );
      ?>
    </ul>
  </div>
</section>
