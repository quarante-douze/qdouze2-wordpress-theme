<?php
$listmenu = get_nav_menu_locations();
$menu = wp_get_nav_menu_items($listmenu['link-menu']);
if ($menu != null) {
?>
<section class="card c-secondary">
  <h2 class="card-header"><svg class="icon" alt=""><use xlink:href="#icon-link"></use></svg> Liens</h2>
  <ul class="card-menu">
    <?php
      $menu = wp_get_nav_menu_items( 'link-menu');
        foreach ($menu as $menuElement) {
          echo '<li><a href="' . $menuElement->url . '" class="menu-element">'. $menuElement->title . '</a></li>';
        }
      ?>
  </ul>
</section>
<?php
}
?>
