<?php $flags = get_terms(
  array(
      'taxonomy'   => 'flag',
      'hide_empty' => false,
  )
);

if ($flags) {
  foreach( $flags as $flag ) {
    if ($flag->slug == "global") {
      $term_meta = get_option( "taxonomy_term_$flag->term_id" );
      echo "<div class='toast bg-soft c-" . $term_meta['niveau'] . "'>";
      echo $flag->description;
      echo "</div>";
    }
  }
} ?>
