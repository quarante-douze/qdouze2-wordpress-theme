<h3 class="sr-only">Tags et catégories</h3>
<?php
  if (has_category('chapters')) {
    echo "<ul class='nolist' aria-label='Romans'>";
    $romans = get_the_terms($post->ID, 'roman');
    foreach( $romans as $roman ) {
      echo "<li><a href= '" . esc_url( get_category_link( $roman->term_id ) ) . "' class='btn btn-small c-primary'><svg class='icon' alt=''><use xlink:href='#icon-folder'></use></svg>&nbsp;" . $roman->name . "</a></li>";
    }
    echo "</ul>";
  } else {
    $categories = get_the_category();
    echo "<ul class='nolist' aria-label='Catégories'>";
    foreach( $categories as $category ) {
      echo "<li><a href= '" . esc_url( get_category_link( $category->term_id ) ) . "' class='btn btn-small c-primary'><svg class='icon' alt=''><use xlink:href='#icon-folder'></use></svg>&nbsp;" . $category->cat_name . "</a></li>";
    }
    echo "</ul>";
  }
  $tags = get_the_tags();
  if ($tags) {
    echo "<ul class='nolist' aria-label='Tags'>";
    foreach( $tags as $tag ) {
      echo "<li><a href= '" . esc_url( get_tag_link( $tag->term_id ) ) . "' class='btn btn-small c-secondary'><svg class='icon' alt=''><use xlink:href='#icon-tags'></use></svg>&nbsp;" . $tag->name . "</a></li>";
    }
    echo "</ul>";
  }
?>
