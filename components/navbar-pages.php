<div class="osd">
  <div class="container menu toolbar fg-light d-none d-flex-sm">
    <nav class="d-flex">
      <h2 class="sr-only">Menu des pages</h2>
      <ul>
        <li>
          <a href="<?php echo site_url(); ?>" class="menu-item">
            <svg class="icon icon-home" alt=""><use xlink:href="#icon-home"></use></svg><span class="sr-only">Accueil</span>
          </a>
        </li>
        <li>
          <a href="#" class="menu-item submenu">
            Pages <svg class="icon icon-caret-down" alt=""><use xlink:href="#icon-caret-down"></use></svg>
          </a>
          <ul class="bg menu fg-dark">
            <?php
            $listmenu = get_nav_menu_locations();
            $menu = wp_get_nav_menu_items($listmenu['top-navbar-2']);
              foreach ($menu as $menuElement) {
                echo '<li><a href="' . $menuElement->url . '" class="menu-item">'. $menuElement->title . '</a></li>';
              }
            ?>
          </ul>
        </li>
        <?php
          $listmenu = get_nav_menu_locations();
          $menu = wp_get_nav_menu_items($listmenu['top-navbar']);
            foreach ($menu as $menuElement) {
              echo '<li><a href="' . $menuElement->url . '" class="menu-item">'. $menuElement->title . '</a></li>';
            }
          ?>
      </ul>
    </nav>

    <ul class="f-end d-none d-flex-sm">
      <li class="f-column f-center"><?php include(TEMPLATEPATH . '/components/searchform.php'); ?></li>
    </ul>
  </div>
</div>
