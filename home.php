<?php get_header(); ?>
<main class="fullwidth" id="skip">
  <?php include(TEMPLATEPATH . '/components/globalflag.php'); ?>

  <!-- Recuperation de la page de home -->
  <?php if ( !is_paged() ) {
    $page = get_page_by_path( 'home' );
      if ($page != null) {
        echo '<section aria-labelledby="title-description">';
        echo '<h1 class="page-title" id="title-description">' . get_the_title( $page ) . '</h1>';
        echo '<div>' . $page->post_content . '</div>';
        echo '</section>';
      }
      echo '<section aria-labelledby="title-featured">';
      echo '<h1 class="page-title" id="title-featured"><svg class="icon icon-star" viewBox="0 0 30 32"><path d="M29.714 11.554c0 0.321-0.232 0.625-0.464 0.857l-6.482 6.321 1.536 8.929c0.018 0.125 0.018 0.232 0.018 0.357 0 0.464-0.214 0.893-0.732 0.893-0.25 0-0.5-0.089-0.714-0.214l-8.018-4.214-8.018 4.214c-0.232 0.125-0.464 0.214-0.714 0.214-0.518 0-0.75-0.429-0.75-0.893 0-0.125 0.018-0.232 0.036-0.357l1.536-8.929-6.5-6.321c-0.214-0.232-0.446-0.536-0.446-0.857 0-0.536 0.554-0.75 1-0.821l8.964-1.304 4.018-8.125c0.161-0.339 0.464-0.732 0.875-0.732s0.714 0.393 0.875 0.732l4.018 8.125 8.964 1.304c0.429 0.071 1 0.286 1 0.821z"></path></svg> À la une</h1>';
      include(TEMPLATEPATH . '/components/featured.php');
      echo '</section>';
    }
  ?>
  <section aria-labelledby="title-publications">
    <h1 class="page-title" id="title-publications"><svg class="icon icon-newspaper-o" alt="" viewBox="0 0 37 30"><path d="M18.286 9.143h-6.857v6.857h6.857v-6.857zM20.571 20.571v2.286h-11.429v-2.286h11.429zM20.571 6.857v11.429h-11.429v-11.429h11.429zM32 20.571v2.286h-9.143v-2.286h9.143zM32 16v2.286h-9.143v-2.286h9.143zM32 11.429v2.286h-9.143v-2.286h9.143zM32 6.857v2.286h-9.143v-2.286h9.143zM4.571 24v-17.143h-2.286v17.143c0 0.625 0.518 1.143 1.143 1.143s1.143-0.518 1.143-1.143zM34.286 24v-19.429h-27.429v19.429c0 0.393-0.071 0.786-0.196 1.143h26.482c0.625 0 1.143-0.518 1.143-1.143zM36.571 2.286v21.714c0 1.893-1.536 3.429-3.429 3.429h-29.714c-1.893 0-3.429-1.536-3.429-3.429v-19.429h4.571v-2.286h32z"></path></svg> Publications</h1>
    <?php include(TEMPLATEPATH . '/components/preview-grid.php'); ?>
  </section>
    <?php if ( !is_paged() ) {
      ?>
        <div class="toast mb-1 c-info bg-soft home-toast"><strong class="m-0"><svg class="icon icon-info-circle" viewBox="0 0 30 30"><path d="M18.286 24.571v-2.857c0-0.321-0.25-0.571-0.571-0.571h-1.714v-9.143c0-0.321-0.25-0.571-0.571-0.571h-5.714c-0.321 0-0.571 0.25-0.571 0.571v2.857c0 0.321 0.25 0.571 0.571 0.571h1.714v5.714h-1.714c-0.321 0-0.571 0.25-0.571 0.571v2.857c0 0.321 0.25 0.571 0.571 0.571h8c0.321 0 0.571-0.25 0.571-0.571zM16 8.571v-2.857c0-0.321-0.25-0.571-0.571-0.571h-3.429c-0.321 0-0.571 0.25-0.571 0.571v2.857c0 0.321 0.25 0.571 0.571 0.571h3.429c0.321 0 0.571-0.25 0.571-0.571zM27.429 16c0 7.571-6.143 13.714-13.714 13.714s-13.714-6.143-13.714-13.714 6.143-13.714 13.714-13.714 13.714 6.143 13.714 13.714z"></path></svg> Note :</strong>  Si vous êtes intéressé pour découvrir plus
        de contenus trouvé sur internet, vous pouvez aussi regarder mon <a href="http://shaarli.kazhnuz.space/">Shaarli</a>, qui sert plus à
        partager des petites découverte rigolote ou intéressantes.</div>
      <?php
    }
  ?>

</main>
<?php get_footer();  ?>
