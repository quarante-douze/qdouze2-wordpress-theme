<?php get_header(); ?> <!-- ouvrir header,php -->
<main id="skip">
  <?php $category = get_category( get_query_var( 'cat' ) );
    kspace_cat_breadcrumb_with_rss(single_cat_title('', false), 'folder-open', 'la catégorie',  get_category_link( $category->cat_ID ) . '/feed' );
  ?>
  <?php include(TEMPLATEPATH . '/components/posts-list.php'); ?>

</main>

<!-- Sidebar custom pour contenir la description -->
<aside class="sidebar">
  <div class="card c-info">
    <div class="card-header"><svg class="icon icon-folder-open" viewBox="0 0 34 32"><path d="M33.554 17c0 0.429-0.268 0.857-0.554 1.179l-6 7.071c-1.036 1.214-3.143 2.179-4.714 2.179h-19.429c-0.643 0-1.554-0.196-1.554-1 0-0.429 0.268-0.857 0.554-1.179l6-7.071c1.036-1.214 3.143-2.179 4.714-2.179h19.429c0.643 0 1.554 0.196 1.554 1zM27.429 10.857v2.857h-14.857c-2.232 0-5 1.268-6.446 2.982l-6.107 7.179c0-0.143-0.018-0.304-0.018-0.446v-17.143c0-2.196 1.804-4 4-4h5.714c2.196 0 4 1.804 4 4v0.571h9.714c2.196 0 4 1.804 4 4z"></path></svg> <?php echo single_cat_title(); ?></div>
    <div class="card-body">
      <?php the_archive_description() ?>
    </div>
  </div>

  <?php include(TEMPLATEPATH . '/components/sidebar-content.php'); ?>
</aside>
<?php get_footer(); ?>
