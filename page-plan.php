<?php /* Template Name: page-plan */ ?>
<?php get_header(); ?> <!-- ouvrir header,php -->
<main id="skip">
  <?php if(have_posts()) : ?><?php while(have_posts()) : the_post(); ?>

  <article class="article-content" id="post-<?php the_ID(); ?>">
  <h1 class="page-title"><?php the_title(); ?></h1>

    <div class="article-body">
      <section class="mb-1">
        <h2>Derniers articles</h2>
        <ul>
        <?php
          wp_get_archives( array(
            'type'  => 'postbypost',
            'echo'  => 1,
            'order' => 'ASC',
            'limit' => 12
          ) );
          ?>
        </ul>
      </section>
      <section class="mb-1">
        <h2>Pages</h2>
        <ul>
          <?php wp_list_pages( 'sort_column=menu_order&depth=1&title_li=' ) ?>
        </ul>
      </section>
      <section class="mb-1">
        <h2>Catégories</h2>
        <ul>
          <?php
          $categories = get_categories( array(
            'orderby' => 'name',
            'order'   => 'ASC'
          ) );

          foreach( $categories as $category ) {
            echo '<li><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></li>';
          }?>
        </ul>
      </section>
      <section class="mb-1">
        <h2>Archive des articles</h2>
        <ul>
          <?php wp_get_archives('type=yearly&format=html&show_post_count=0'); ?>
        </ul>
      </section>
    </div>
  </article>

  <?php endwhile; ?>
  <?php endif; ?>
</main>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
