<?php get_header(); ?> <!-- ouvrir header,php -->
<main id="skip">
    <?php $tag = get_category( get_query_var( 'tag' ) );
      kspace_cat_breadcrumb_with_rss(single_cat_title('', false), 'tag', 'RSS du tag',  get_tag_link( $tag->cat_ID ) . '/feed' );
    ?>
    <?php include(TEMPLATEPATH . '/components/preview-list.php'); ?>
</main>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
